=== Simply Zen ===
Contributors: Wayne Connor, Sunil Kumar
Requires at least: 6.1
Tested up to: 6.3
Requires PHP: 5.6
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Statue Image for theme screenshot 
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/820715

Mat Image for theme screenshot
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/694976

Stones Image for theme screenshot
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/935798

Main Logo Image for theme screenshot
License: CC0 1.0 Universal (CC0 1.0)
License URL: https://pxhere.com/en/license
Source: https://pxhere.com/en/photo/989976

These images are released free of copyrights under Creative Commons CC0.
You may download, modify, distribute, and use them royalty free for anything you like, even in commercial applications. Attribution is not required.

== Description ==

Introducing "Simply Zen"

Let your content shine with a lovely clean and clutter-free design. Inspired by Steve Jobs, Apple, and the minimalist design ethos, we created a theme that is both beautiful and functional, embodying the essence of simplicity.



== Changelog ==
= 1.0 = Original Release
= 1.1 = design updates
= 1.2 = image updates
= 1.3 = image copyright updates
= 1.4 = Post default image and search in header
= 1.5 = Fix bugs
= 1.6 = Fix bugs
= 1.9 = example pasts template
= 1.10 = trying to work out how to change what is seen in the wordpres.org 'preview'
= 1.11= fixed screenshot
= 1.12= update wordpress description
= 1.13= Fix capitalisation issue. Centre single posts in phone view.
= 1.14= Revert Theme.json to previous working version. 1.13 changes still current.

* Released: July 1, 2023


== Copyright ==

Copyright 2020 The Inter Project Authors
Source: https://github.com/rsms/inter
This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL  

Simply Zen WordPress Theme, Copyright 2023 Wayne Connor
Simply Zen is distributed under the terms of the GNU GPL, Simply Zen WordPress Theme is derived from Twenty Twenty Three WordPress Theme, Copyright 2023 Wordpress.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
