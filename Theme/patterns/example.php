<?php
/**
 * Title: Example
 * Slug: simply-zen/example
 * Categories:  simply-zen
 * Keywords: example
 *
 * @package  simply-zen
 */

?>

<style>
.main_page {
  flex: 1;
}

.post_template {
  padding-left: 0px;
}

.blogs h1 {
  font-size: 72px;
  font-weight: 500;
  margin: 50px 0;
  display: block;
}

.blogs .post_template li {
  display: inline-block;
  width: calc(33.33% - 24px);
  box-shadow: 0 0 20px #ccc;
  padding: 13px;
  box-sizing: border-box;
  border-radius: 10px;
  margin: 0 10px;
 
}

.blogs .post_template li img {
  width: 100%;
  height: 250px;
  object-fit: cover;
  border-radius: 10px;   
}

.blogs .post_template li .post_title a {
	text-decoration: none;
	color: #000;
	font-size: 24px;
	font-weight: 500;
	line-height: 30px;
	display: block;
	margin-top: 20px;
	min-height: 90px;
}
.more-Reading {
  font-size: 18px;
  color: #000;
  margin-bottom: 20px;
  display: block;
}

.post_date {
  width: calc(50% - 3px);
  display: inline-block;
  margin-bottom: 20px;
}

.tag_s {
  width: calc(50% - 3px);
  display: inline-block;
  text-align: right;
  margin-bottom: 20px;
  color: #000;
}

.container {
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
}

 

@media only screen and (max-width: 600px) {
  .blogs .post_template li {
    width: 100%;
    margin: 10px 0px;
  }

  .blogs h1 {
    font-size: 48px;
  }


  .blogs .post_template li .post_title a {
	min-height: auto;
  }
 
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
  .blogs .post_template li {
    width: calc(50% - 23px);
    margin: 10px 10px;
  }
}


</style>




    <div class="main_page">
      <div class="blogs">
        <div class="container">
         <div class="image-heading-container">

        <img width="300" src="<?php echo esc_url( get_theme_file_uri( 'assets/images/stones.jpg' ) ); ?>"  class="custom-logo" alt="Simply Zen" decoding="async" fetchpriority="high">
        
        <h1>Simply Zen</h1>
    </div>
          <ul class="post_template">
            <li>
              <img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/pxhere-820715.jpg' ) ); ?>" alt="">
              <h2 class="post_title"><a href="" target="_self" rel="noopener">3 Example posts</a></h2>
              <p class="post_descrption">These first three posts are example posts that come with the theme. You can remove them by going to 'site editor' and removing this block. </p>
              <a class="more-Reading" href="">Continue Reading</a>
              <div class="post_date">August 1, 2023</div>
              <a class="tag_s" href="">Wordpress</a>
            </li>
            <li>
              <img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/pxhere-694976.jpg' ) ); ?>" alt="">
              <h2 class="post_title"><a href="" target="_self" rel="noopener">You can add your own image for each post</a></h2>
              <p class="post_descrption">Your posts should display below these three posts. Do they display an image at the top? If not you'll need to select some featured images.</p>
              <a class="more-Reading" href="">Continue Reading</a>
              <div class="post_date">July 30, 2023</div>
              <a class="tag_s" href="">hardware</a>
            </li>
            <li>
              <img src="<?php echo esc_url( get_theme_file_uri( 'assets/images/pxhere-935798.jpg' ) ); ?>" alt="">
              <h2 class="post_title"><a href="" target="_self" rel="noopener">Set 'Featured Images'</a></h2>
              <p class="post_descrption">To set a featured image for a post just open the wordpress post editor for each post and upload or select a featured image for each post.  </p>
              <a class="more-Reading" href="">Continue Reading</a>
              <div class="post_date">July 28, 2023</div>
              <a class="tag_s" href="">sync</a>
            </li>
          </ul>
        </div>
      </div>
    </div>


<!-- /wp:group -->
<!-- /wp:group -->
